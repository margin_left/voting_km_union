from rest_framework.viewsets import ReadOnlyModelViewSet

import models
import serializers

class VotingView(ReadOnlyModelViewSet):
    queryset = models.VotingModel.objects.all()
    serializer_class = serializers.VotingSerializer


class NomineeView(ReadOnlyModelViewSet):
    queryset = models.NomineeModel.objects.all()