from django.db import models
from django.utils.timezone import datetime, timedelta
from django.core.validators import MinValueValidator

class NomineeModel(models.Model):
    name = models.CharField(max_length=255, unique=True)

class VotingModel(models.Model):
    start_date = models.DateField(default=datetime.today())
    end_date = models.DateField(default=(datetime.today() + timedelta(days=3)))
    max_votes_for_win = models.IntegerField(null=True, validators=(MinValueValidator(1)))

class VoteModel(models.Model):
    nominee = models.ForeignKey(NomineeModel, on_delete=models.SET_NULL)
    voting = models.ForeignKey(VotingModel, on_delete=models.SET_NULL)
    vote_moment = models.DateTimeField(default=datetime.now())

class VotingProtocol(models.Model):
    current_votes = models.IntegerField(default=0, validators=(MinValueValidator(0)))
    nominee = models.ForeignKey(NomineeModel, on_delete=models.PROTECT)
    voting = models.ForeignKey(VotingModel, on_delete=models.PROTECT)

