from django.utils import timezone
from rest_framework.serializers import ModelSerializer, SerializerMethodField
import models

class VotingSerializer(ModelSerializer):
    is_active = SerializerMethodField()

    class Meta:
        model = models.VotingModel
        field = ['id', 'start_date', 'end_date', 'max_votes_for_win']

    def get_is_active(self, obj: models.VotingModel):
        if obj.start_date > timezone.now() or obj.end_date < timezone.now():
            return False

        protocol = models.VoteModel.objects.filter(voting=obj.pk)
        if protocol.count() >= obj.max_votes_for_win:
            return False

        return True